#!/usr/bin/env ruby

ERROR_TEXT = {
	ad_not_callable: "third param of alias declaration must be Callable",
	boolarg_bad_str: "something not in [false, true, 0, 1, \"0\", \"1\"] passed to BoolArg.format",
	builtin_default_new: "call to constructor on Builtin descendant where not redefined",
	builtin_default_acc: "call to accepts?(o) on Builtin descendant where not redefined",
	builtin_default_fmt: "call to format(o) on Builtin descendant where not redefined",
	cad_contains_quotes: "cannot nest quotes, as required to correctly alias",
	cl_invalid_arg: "command does not accept arguments of that type and/or format",
	cl_no_such_cmd: "command is not builtin and is not the name of a defined alias",
	cl_use_aliasdecl: "attempted to make alias w/ Callable instead of AliasDeclaration",
	mcl_contains_quotes: "cannot nest quotes, as required to correctly alias command chain"
}

class Builtin
	@c = false
	def initialize; raise ERROR_TEXT[:builtin_default_new] end
	def accepts?(o) raise ERROR_TEXT[:builtin_default_acc] end
	def c_aliasable?; @c end
	def format(o) raise ERROR_TEXT[:builtin_default_fmt] end

	class NoArg < Builtin # don't instantiate, use constant
		def initialize; @c = true end
		def accepts?(o) o.nil? end
		def format(o) nil end
	end
	NO_ARG = [NoArg.new]
	class PlusNoArg < NoArg; end # don't instantiate, use constant
	PLUS = PlusNoArg.new

	class BoolArg < Builtin # don't instantiate, use constants
		F = proc {|o| (o.is_a?(FalseClass) || o.is_a?(TrueClass) ? o : !(o.is_a?(Integer) ? o : o.to_i).zero?) ? 1 : 0}
		R = 0..1
		def initialize; end
		def accepts?(o)
			return true if o.is_a?(FalseClass) || o.is_a?(TrueClass)
			return R.cover?(o.is_a?(Integer) ? o : (o.is_a?(String) ? o.to_i : raise(ERROR_TEXT[:boolarg_bad_str])))
		end
		def format(o) F.call(o) end
	end
	BOOL_T = [BoolArg.new, 1]
	BOOL_F = [BoolArg.new, 0]

	class StringArg < Builtin # don't instantiate, use constant
		E = "\"\""
		def initialize; end
		def accepts?(s) s.is_a?(String) end
		def format(o) s = o.to_s; s.empty? ? E : s end
	end
	STRING = StringArg.new
	class QuotedStringArg < StringArg # don't instantiate, use constant
		def format(s) "\"#{s}\"" end
	end
	QUOTED_STRING = QuotedStringArg.new
	class FreeStringArg < StringArg # don't instantiate, use constant
		def format(s) s end
	end
	FREE_STRING = FreeStringArg.new
	class TF2EntityNameArg < FreeStringArg # don't instantiate, use constant
		def accepts?(s) VERSIONED_ENTITY_NAMES[:TF2].include? s end
	end
	TF2_ENTITY_NAME = [TF2EntityNameArg.new]
	class NextBotNameArg < QuotedStringArg # don't instantiate, use constant
		def accepts?(s) VERSIONED_MISC[:TF2_NEXTBOT_NAMES].include? s end
	end
	TF2_NEXTBOT_NAME = [NextBotNameArg.new]
	class StringEnumArg < FreeStringArg
		def initialize(*e) @e = e end
		def accepts?(s) @e.include? s end
	end

	class NumericArg < FreeStringArg
		def initialize; end
		def accepts?(n) FLOAT.accepts?(n) end
	end
	NUMERIC = NumericArg.new
	class BoundedNumericArg < NumericArg
		def initialize(r) @range = r end
		def accepts?(n) NUMERIC.accepts?(n) && @range.cover?(n.to_f) end
	end

	class IntArg < NumericArg # don't instantiate, use constant
		RE = /[1-9]\d+/
		def accepts?(n) n.is_a?(Integer) || (n.is_a?(String) && n.match?(RE)) end
	end
	INT = IntArg.new
	class BoundedIntArg < IntArg
		def initialize(r) @range = r end
		def accepts?(n) INT.accepts?(n) && @range.cover?(n.to_i) end
	end
	# "byte" - if given value outside this range, uses value modulo 256
	BYTE = BoundedIntArg.new 0..255
	class IntWhitelist < IntArg
		def initialize(a) @allow = a end
		def accepts?(n) INT.accepts?(n) && @allow.include?(n) end
	end
	WLIST_012 = IntWhitelist.new [0, 1, 2]
	WLIST_0123 = IntWhitelist.new [0, 1, 2, 3]

	# "float" - if given integral value w/o .0 suffix, will still work but will show in differences
	class FloatArg < NumericArg # don't instantiate, use constant
		F = proc {|o| n = o.is_a?(String) ? o.to_f : o; n.is_a?(Float) ? n : "%.1f"%n}
		RE = /[1-9]\d*(\.\d+)?|0\.\d+/
		def accepts?(n) n.is_a?(Float) || n.is_a?(Integer) || (n.is_a?(String) && n.match?(RE)) end
		def format(o) F.call(o) end
	end
	FLOAT = FloatArg.new
	class MarkedFloatArg < FloatArg # don't instantiate, use constant
		def accepts?(s) n = s[0..-1].to_i; s[-1] == "f" && (FLOAT.accepts?(n)) end
		def format(o) "#{F.call(o)}f" end
	end
	MARKED_FLOAT = MarkedFloatArg.new
	class BoundedFloatArg < FloatArg
		def initialize(r) @range = r end
		def accepts?(n) FLOAT.accepts?(n) && @range.cover?(n.to_f) end
		def format(o) F.call(o) end
	end

	class MultiArg < Builtin
		def initialize(b, *a) @i = b; @checks = a end
		def accepts?(a) a.each.with_index {|o, i| return false unless @checks[i].accepts? o}; true end
		def format(a) a.map.with_index {|o, i| @checks[i].format o} * " " end
		def inlineable?; @i end
	end

	# don't instantiate the following, use constants
	class AliasName < StringArg
		REGEX = /^[\x21\x23-\x26\x2A-\x2E0-9\x3C-\x5B\x5D-\x5Fa-z\x7C]{1,31}$/
		def accepts?(s) s.match? REGEX end
	end
	ALIAS_NAME = [AliasName.new]
	class AliasArgs < MultiArg
		def initialize; @checks = [ALIAS_NAME, STRING] end
		def format(o) o end
	end
	ALIAS = [AliasArgs.new]
	class Echo < FreeStringArg
		REGEX = /^[^\\"]+$/
		def accepts?(o) o.nil? || o.match?(REGEX) end
	end
	ECHO = [Echo.new]
	FIND = ECHO
	SAY = ECHO
	class Exec < StringArg
		REGEX = /^[-.0-9_a-z]+(\.cfg)?$/
		def accepts?(s) s.match? REGEX end
	end
	EXEC = [Exec.new]

	class DunnoArg < StringArg
		SURE = "Sure, why not."
		def accepts?(o) SURE end
	end
	DUNNO = DunnoArg.new
end
PLUS_MINUS_MAP = {}
VERSIONS = {TRESS: [0, 19]}
GAMES = {
	"gmod" => :GMod,
	"portal2" => :Portal2,
	"tf2" => :TF2
}
VERSIONED_COMMANDS = {}
VERSIONED_ENTITY_NAMES = {}
VERSIONED_MISC = {}
require_relative "tdata/versioned_common"
GAMES.each {|g, _| require_relative "tdata/versioned_#{g}"}

class NamePair
	attr_reader :name, :sh
	def initialize(name, sh)
		@name = name
		@sh = sh
	end
	def to_a; @sh.nil? ? [@name] : [@name, @sh] end
end
class Callable
	def initialize(a, s) @a = a; @s = s end
	def aliasable?; @a end
	def cmd_data(np, n)
		return VERSIONED_COMMANDS[:C][n] if VERSIONED_COMMANDS[:C].keys.include? n
		return VERSIONED_COMMANDS[GAME][n] if VERSIONED_COMMANDS[GAME].keys.include? n
		ALIAS_DEPS[np].each {|d| return Builtin::NO_ARG if ALIASES[d].include? n.to_s}
		raise ERROR_TEXT[:cl_no_such_cmd]
	end
	def to_s; @s end
end
class ConstructedCallable < Callable
	A = [Builtin::NoArg, Builtin::BoolArg, Builtin::FreeStringArg]
	F = proc {|s| s.start_with?("plus_") ? s.sub("plus_", "+") :
		(s.start_with?("minus_") ? s.sub("minus_", "-") : s.delete_prefix("empty_"))}
	def initialize(namespace, cmd_name, *args_a)
		raise ERROR_TEXT[:cl_use_aliasdecl] if cmd_name == :alias
		cmd = cmd_data(namespace, cmd_name)[0]
		args = args_a.empty? ? nil : (args_a.length == 1 ? args_a[0] : args_a)
		raise ERROR_TEXT[:cl_invalid_arg] unless cmd.accepts? args
		arg_str = cmd.format args
		super(calc_aliasable(cmd), if arg_str.nil?
			F.call(cmd_name.to_s)
		else
			arg_str = arg_str.to_s
			arg_str = Builtin::StringArg::E if arg_str.empty?
			"#{F.call(cmd_name.to_s)} #{arg_str}"
		end)
	end
	def calc_aliasable(c)
		return c.inlineable? if c.is_a?(Builtin::MultiArg)
		A.each {|t| return true if c.is_a?(t)}
		return false
	end
end
class MultiCallable < Callable
	def initialize(callables)
		callables.each {|c| raise ERROR_TEXT[:mcl_contains_quotes] unless c.aliasable?}
		super(true, callables * "; ")
	end
end
class AliasCallable < Callable; def initialize(a, s, ns, n) super(a, s); ALIASES[ns] << n end end
class AliasDeclaration < AliasCallable
	def initialize(namespace, name, call)
		raise ERROR_TEXT[:ad_not_callable] unless call.is_a?(Callable) && call.aliasable?
		super(false, "alias #{name} \"#{call}\"", namespace, name)
	end
end
class CallableAliasDeclaration < AliasCallable
	def initialize(namespace, name, cmd_name)
		raise ERROR_TEXT[:cad_contains_quotes] unless cmd_data(namespace, cmd_name)[0].c_aliasable?
		super(true, "alias #{name} #{cmd_name}", namespace, name)
	end
end
def np(n, s = nil) NamePair.new(n, s) end
def mcl(*cls) MultiCallable.new(cls) end
def ad(ns, name, cmd) AliasDeclaration.new(ns, name, cmd) end
def cad(ns, name, cmd_name) CallableAliasDeclaration.new(ns, name, cmd_name) end
def new_type_hash(**o)
	h = {raws: [], throwables: [], actions: [], bools: [], roulettes: [], values: [], subsections: []}
	o.each {|k, v| h[k] = v}
	h
end
class AliasOrSection
	attr_reader :name, :names, :old_type, :subs, :sub_subs
	def initialize(old_type, ns, np)
		@names = np.to_a
		@name = np.name
		@no_shorthand = @names.length == 1
		@ns = ns
		@old_type = old_type
	end
end
class ActionAlias < AliasOrSection
	def initialize(ns, np, call, s)
		super(:actions, ns, np)
		@call = call
		@s = s
	end
	def gen_shorthand(a)
		return nil if @no_shorthand
		n = a * "." + "."
		return [ad(@ns, "#{n}#{@names[1]}*", @s.cl("#{@n}")), ad(@ns, "#{n}#{@names[1]}", @s.cl("#{@n}"))] * "\n"
	end
	def gen_sub_data(a) @n = "#{a * "."}*"; @subs = ad(@ns, @n, @call) end
end
class BoolAlias < AliasOrSection
	H = {"-" => "0", "+" => "1", "%" => "%"}
	def initialize(ns, np, off_cmd, on_cmd, s, init: false)
		super(:bools, ns, np)
		@cmds = [off_cmd, on_cmd]
		@s = s
		@init = init ? 1 : 0
	end
	def gen_shorthand(a)
		return nil if @no_shorthand
		n = a * "." + "."
		return [ad(@ns, "-#{n}#{@names[1]}", @s.cl("#{@n}-")), ad(@ns, "+#{n}#{@names[1]}", @s.cl("#{@n}+")),
			ad(@ns, "%#{n}#{@names[1]}", @s.cl("#{@n}%"))] * "\n"
	end
	def gen_sub_data(a)
		n = a * "."
		@subs = []
		[0, 1].each {|al| ALIASES[@ns] << "#{n}>#{al}" << "#{n}<#{al}"}
		H.each {|k, v| ALIASES[@ns] << n + k << k + n}
		@cmds.each.with_index {|c, i| @subs << ad(@ns, "#{n}>#{i}", c) << ad(@ns, "#{n}<#{i}",
			mcl(cad(@ns, "#{n}>", @s.cl("#{n}>#{i}")), cad(@ns, "#{n}<%", @s.cl("#{n}<#{1 - i}"))))}
		H.each {|k, v| @subs << ad(@ns, n + k, mcl(@s.cl("#{n}<#{v}"), @s.cl("#{n}>"))) << ad(@ns, k + n, @s.cl(n + k))}
		@subs << "#{n}<#{@init}"
		@n = n
	end
end
class RouletteAlias < AliasOrSection
	def initialize(ns, np, cmds, s)
		super(:roulettes, ns, np("@#{np.name}", "@#{np.sh}"))
		@cmds = cmds
		@s = s
	end
	def gen_shorthand(a)
		return nil if @no_shorthand
		n = a * "." + "."
		return [ad(@ns, "-#{n}#{@names[1]}", @s.cl("#{@n}-")), ad(@ns, "+#{n}#{@names[1]}", @s.cl("#{@n}+"))] * "\n"
	end
	def gen_sub_data(a)
		n = a * "."
		@subs = []
		l = @cmds.length - 1
		0.upto(l) {|i| ALIASES[@ns] << "#{n}>#{i}" << "#{n}<#{i}"}
		ALIASES[@ns] << "#{n}@A" << "#{n}@B" << "#{n}>" << "#{n}<"
		0.upto(l - 1) {|i| @subs << ad(@ns, "#{n}>#{i}", @cmds[i]) << ad(@ns, "#{n}<#{i}",
			mcl(cad(@ns, "#{n}>", @s.cl("#{n}>#{i}")), cad(@ns, "#{n}<", @s.cl("#{n}<#{i.succ}"))))}
		@subs << ad(@ns, "#{n}>#{l}", @cmds[l]) << ad(@ns, "#{n}<#{l}",
			mcl(cad(@ns, "#{n}>", @s.cl("#{n}>#{l}")), cad(@ns, "#{n}<", @s.cl("#{n}<0"))))
		@subs << ad(@ns, "#{n}@A", mcl(@s.cl("#{n}<"), @s.cl(:wait, 1), @s.cl("#{n}@B"))) <<
			ad(@ns, "#{n}@B", @s.cl("tress.roulette_intern_call^")) <<
			ad(@ns, "#{n}>", @s.cl("tress.roulette_intern_call^")) << ad(@ns, "#{n}<", @s.cl("#{n}<0")) <<
			ad(@ns, "#{n}-", cad(@ns, "#{n}@B", @s.cl("#{n}>"))) <<
			ad(@ns, "#{n}+", mcl(cad(@ns, "#{n}@B", @s.cl("#{n}@A")), @s.cl("#{n}@A")))
		@n = n
	end
end
class ThrowableAlias < AliasOrSection
	def initialize(ns, np, msg, s, is_fatal = false)
		super(:throwables, ns, np)
		@echo = s.cl(:echo, "[TRESS|#{is_fatal ? "F" : "E"}] #{msg}")
	end
	def gen_sub_data(a) @subs = ad(@ns, "#{a * "."}^", @echo) end
end
class ValueAlias < AliasOrSection
	attr_reader :map
	def initialize(ns, np, cmd_gen, map, s, init: nil)
		super(:values, ns, np)
		@cmd_gen = cmd_gen
		@map = map
		@s = s
		@init = init
	end
	def gen_shorthand(a)
		return nil if @no_shorthand
		n = a * "." + "."
		return @map.map {|k, v| ad(@ns, "#{n}#{@names[1]}=#{k}", @s.cl("#{@n}=#{k}"))} * "\n"
	end
	def gen_sub_data(a)
		@n = a * "."
		@subs = @map.map {|k, v| ad(@ns, "#{@n}=#{k}", @cmd_gen.call(k, v))}
		@subs << "#{@n}=#{@init}" unless @init.nil?
	end
end
class SliderAlias < ValueAlias
	def initialize(ns, np, cmd_gen, range, step, map_gen, s, init: nil)
		v = {}
		range.min.step(range.max, step) {|n| a = map_gen.call(n); v[a[0]] = a[1]}
		super(ns, np, cmd_gen, v, s, init: init)
	end
end
class PercentageSliderAlias < SliderAlias
	def initialize(ns, np, cmd_gen, pc_step, s, zero_tag: nil, one_tag: nil, init: nil)
		super(ns, np, cmd_gen, 0..100, pc_step, proc {|n| [
			n > 0 ? (n < 100 || one_tag.nil? ? "#{n}%" : one_tag) : (zero_tag.nil? ? "0%" : zero_tag),
			"%03.1f"%(n / 100.0)]}, s, init: init)
	end
end
class RawLineAlias < AliasOrSection
	def initialize(ns, np, d)
		super(:raws, ns, np)
		@data = d
	end
	def gen_sub_data(a) @subs = @data end
end
class RawEchoAlias < RawLineAlias
	def initialize(ns, np, t, f, a, s) super(ns, np, mcl(*a.unshift("[TRESS|#{t}] #{f}").map {|e| s.cl(:echo, e)})) end
	def gen_sub_data(a)
		n = a * "."
		@subs = ad(@ns, n, @data)
	end
end
class AliasSection < AliasOrSection
	ALIAS_SORT = proc {|a, b| a.name.delete("!") <=> b.name.delete("!")}
	SORT = proc {|a, b| a.delete("!") <=> b.delete("!")}
	TYPE_MAP = {raws: proc {|c| c}, throwables: proc {|c| "#{c}^"}, actions: proc {|c| "#{c}*"}, bools: proc {|c| "#{c}%"}, roulettes: proc {|c| c}, values: proc {|c| "#{c}="}, subsections: proc {|c| "#{c}|"}}
	attr_reader :ns, :sub_a, :sub_l
	def initialize(ns, np, sub_a)
		super(:subsections, ns, np)
		@ns = ns
		@sub_a = sub_a
		@sub_a[:subsections] = {}
		@sub_l = new_type_hash
		yield self if block_given?
	end
	def action(n, c) @sub_l[:actions] << n; @sub_a[:actions] << ActionAlias.new(@ns, n, c, self) end
	def cl(cmd_name, *args) ConstructedCallable.new(@ns, cmd_name, *args) end
	def create_bool(n, c0, c1, init: nil)
		@sub_l[:bools] << n
		@sub_a[:bools] << BoolAlias.new(@ns, n, c0, c1, self, init: init)
	end
	def gen_shorthand; @sub_a[:subsections].map do |k, v|
		v.gen_shorthand_sub([v.names[1]], []) * "\n" unless v.names.length == 1
	end.compact end
	def gen_shorthand_sub(a, out)
		[:actions, :bools, :roulettes, :values].each {|t| out << @sub_a[t].map {|c| c.gen_shorthand(a)}.compact}
		@sub_a[:subsections].each {|k, v| v.gen_shorthand_sub(a + [k], out)}
		return out
	end
	def gen_sub_data(name_a = [])
		@sub_subs = process_subs(name_a)
		@subs = []
		unless name_a.empty?
			lists = TYPE_MAP.map {|t, p| @sub_l[t].empty? ? nil : (@sub_l[t].map(&:name).map(&p).sort(&SORT)) * " ."}
			unless lists.compact!.empty?
				lists = gen_sub_list(name_a, lists)
				lists.unshift("\n\n") if name_a.length == 1
				@subs << [lists]
			end
			@subs << [@sub_a[:raws].map(&:subs)] unless @sub_a[:raws].empty?
			@subs << [@sub_a[:actions].sort(&ALIAS_SORT).map(&:subs)] unless @sub_a[:actions].empty?
			@subs += @sub_a.reject {|k, v| [:raws, :actions, :subsections].include?(k) || v.empty?}
				.map {|k, v| v.sort(&ALIAS_SORT).map(&:subs)}
		end
		@subs << [@sub_a[:subsections].values
			.map {|s| s.gen_sub_data(name_a + [s.name]); s.subs}] unless @sub_a[:subsections].empty?
		@subs.map! {|g| g.map {|s| s.is_a?(Array) ? s * "\n" : s.to_s} * "\n\n"}
		@subs *= "\n\n\n\n"
	end
	def gen_sub_list(name_a, lists)
		n = name_a * "."
		return [ad(@ns, "#{n}|",
			mcl(cl(:echo, "[TRESS|L] #{n} commands and subgroups"), *lists.map {|e| cl(:echo, ".#{e}")}))]
	end
	def pc_slider(n, c, p, **o)
		@sub_l[:values] << n
		@sub_a[:values] << PercentageSliderAlias.new(@ns, n, c, p, self, o)
	end
	def plus_bool(n, c) create_bool(n, cl(PLUS_MINUS_MAP[c]), cl(c)) end
	def process_subs(name_a)
		lists = new_type_hash
		sub_subs = {}
		@sub_a.each do |k, v| (v.is_a?(Array) ? v : []).each do |s|
			s.gen_sub_data(name_a + [s.name])
			lists[s.old_type] << (s.old_type == :values ? s.names + [s.map.keys] : s.names)
			sub_subs[s.name] = s.sub_subs unless s.sub_subs.nil?
		end end
		return [@names[1], lists, sub_subs]
	end
	def raw_cmd(c) @sub_a[:raws] << RawLineAlias.new(@ns, np(nil), c) end
	def raw_help(n, f, a) @sub_l[:raws] << n; @sub_a[:raws] << RawEchoAlias.new(@ns, n, "H", f, a, self) end
	def raw_print(n, f, a = []) @sub_l[:raws] << n; @sub_a[:raws] << RawEchoAlias.new(@ns, n, "P", f, a, self) end
	def roulette(n, a) @sub_l[:roulettes] << n; @sub_a[:roulettes] << RouletteAlias.new(@ns, n, a, self) end
	def slider(n, c, r, s, g, **o)
		@sub_l[:values] << n
		@sub_a[:values] << SliderAlias.new(@ns, n, c, r, s, g, self, o)
	end
	def subsection(n, a = new_type_hash, &blk)
		@sub_l[:subsections] << n
		s = AliasSection.new(@ns, n, a, &blk)
		@sub_a[:subsections][n.name] = s
		return s
	end
	def throwable(n, m, f = nil)
		@sub_l[:throwables] << n
		@sub_a[:throwables] << ThrowableAlias.new(@ns, n, m, self, f)
	end
	def value(n, c, m, **o) @sub_l[:values] << n; @sub_a[:values] << ValueAlias.new(@ns, n, c, m, self, o) end
	def wrapped_bool(n, c, swap: false, **o) create_bool(n, cl(c, swap ? 1 : 0), cl(c, swap ? 0 : 1), o) end
end

puts "TRESS Architect version #{VERSIONS[:TRESS] * "."}"
if ARGV.empty? or !GAMES.keys.include?(ARGV.first)
	puts "Provide game as first argument (e.g. `./build.rb tf2 [build targets]...`)"
	puts "List of games: #{GAMES.keys * " "}"
	exit true
end
GAME_NAME = ARGV.shift
GAME = GAMES[GAME_NAME]
ALIAS_DEPS = {}
ALIASES = {tress: ["tress.roulette_intern_call^"]} # Required for roulettes
TARGET_DATA = {C: {}}
WRITABLE_DATA = {C: {}}
GAMES.each {|_, g| TARGET_DATA[g] = {}; WRITABLE_DATA[g] = {}}
def update_tdata(g, k, v)
	v.flatten.uniq.each {|w| [ALIAS_DEPS, ALIASES, WRITABLE_DATA[g]].each {|h| h[w] = [] unless h.keys.include? w}}
	TARGET_DATA[g][k] = v
	TARGET_DATA[g].each {|_, d| d.each.with_index {|w, i| (0..i).each {|j| ALIAS_DEPS[w] << v[j]}}}
end
["common", GAME_NAME].each {|g| Dir.glob("tdata/#{g}/[^_]*.rb").sort.each {|f| require_relative f}}
if ARGV.empty?
	puts "Provide build targets as arguments (e.g. `./build.rb #{GAME_NAME} stock_defaults tress_canonical`)"
	puts "Available targets: #{TARGET_DATA[GAMES[GAME_NAME]].keys * " "}"
	puts "Alternatively, use just the keyword \"all\" to build all targets"
	exit true
end
def write_dataset(target, dataset)
	File.write(["output", GAME_NAME, "#{dataset}.cfg"] * File::SEPARATOR,
		WRITABLE_DATA[(WRITABLE_DATA[:C].keys.include?(dataset) ? :C : GAME)][dataset].map do |s|
				s.is_a?(Array) ? s.flatten * "\n" : s.to_s
			end.unshift("// #{target} built by TRESS v#{VERSIONS[:TRESS] * "."}-#{VERSIONS[GAME] * "."}") *
				"\n\n\n\n" + "\n")
	return dataset
end
if ARGV == ["all"]
	puts "Building all targets..."
	written = []
	TARGET_DATA[GAME].each {|k, v| v.each {|d| written << write_dataset(k, d) unless written.include? d}}
	exit true
end
to_write = ARGV.map(&:downcase).select {|t| b = TARGET_DATA.keys.include?(t); puts "No such target #{t}!" unless b; b}
written = []
to_write.each do |t|
	puts "Building target #{t}..."
	TARGET_DATA[GAME][t].each {|d| written << write_dataset(t, d) unless written.include? d}
end
