update_tdata(:TF2, "stock_defaults", [:stock_defaults])

ignored_builtins = [Builtin::AliasArgs, Builtin::Echo, Builtin::TF2EntityNameArg, Builtin::Exec, Builtin::MultiArg,
	Builtin::NextBotNameArg, Builtin::NoArg, Builtin::PlusNoArg]
w = AliasSection.new(:stock_defaults, np(nil), new_type_hash) do |w|
	w.subsection(np(nil)) do |s|
		VERSIONED_COMMANDS[:TF2].reject {|k, v| ignored_builtins.include? v[0].class}
			.each {|k, v| s.raw_cmd(s.cl(k, v.length > 2 ? v[2] : v[0].format(v[1])))}
	end
end
w.gen_sub_data
WRITABLE_DATA[:TF2][:stock_defaults] << w.subs
