#SELF_TWICE = proc {|s| [s, s]}

update_tdata(:TF2, "tress_canonical", [:tress, :tress_tf2, :x_nextbot_kickall, :x_nextbot_killall])
ALIASES[:tress_tf2] << "tress.roulette_intern_call^" # Overwritten by call to update_tdata

w = AliasSection.new(:tress_tf2, np(nil), new_type_hash)
w.subsection(np("&admin", "&ad")) do |s|
	s.action(np("swap_teams", "sw_t"), s.cl(:mp_switchteams))
	s.value(np("autobalance", "ab"),
		proc {|k, v| s.cl(:mp_autoteambalance, v)}, {"off" => 0, "ask" => 2, "force" => 1})
	s.slider(np("autobalance_diff", "ab_df"), proc {|k, v| s.cl(:mp_teams_unbalance_limit, v)}, 2..4, 1, SELF_TWICE)
end
w.subsection(np("audio", "au")) do |s|
	s.subsection(np("hitsound", "hs")) do |s1|
		s1.wrapped_bool(np("enabled", "en"), :tf_dingalingaling, init: true)
		s1.slider(np("max_pitch", "mx_ptc"), proc {|k, v| s.cl(:tf_dingaling_pitchmaxdmg, v)},
			-1..255, 16, proc {|n| n < 0 ? ["01", 1] : ["%02X"%n, n]})
		s1.slider(np("min_pitch", "mn_ptc"), proc {|k, v| s.cl(:tf_dingaling_pitchmindmg, v)},
			-1..255, 16, proc {|n| n < 0 ? ["01", 1] : ["%02X"%n, n]})
		s1.value(np("sound", "sfx"), proc {|k, v| s.cl(:tf_dingalingaling_effect, v)}, {"default" => 0, "electro" => 1,
			"notes" => 2, "perc" => 3, "retro" => 4, "space" => 5, "beepo" => 6, "vortex" => 7, "squasher" => 8})
	end
	s.subsection(np("killsound", "ks")) do |s1|
		s1.wrapped_bool(np("enabled", "en"), :tf_dingalingaling_lasthit, init: true)
		s1.slider(np("max_pitch", "mx_ptc"), proc {|k, v| s.cl(:tf_dingaling_lasthit_pitchmaxdmg, v)},
			-1..255, 16, proc {|n| n < 0 ? ["01", 1] : ["%02X"%n, n]})
		s1.slider(np("min_pitch", "mn_ptc"), proc {|k, v| s.cl(:tf_dingaling_lasthit_pitchmindmg, v)},
			-1..255, 16, proc {|n| n < 0 ? ["01", 1] : ["%02X"%n, n]})
		s1.value(np("sound", "sfx"), proc {|k, v| s.cl(:tf_dingalingaling_last_effect, v)},
			{"default" => 0, "electro" => 1, "notes" => 2, "perc" => 3, "retro" => 4, "space" => 5,
				"beepo" => 6, "vortex" => 7, "squasher" => 8})
	end
	s.subsection(np("mixer", "mx")) do |s1|
		s1.pc_slider(np("hitsound", "hs"), proc {|k, v| s.cl(:tf_dingaling_volume, v)}, 10, zero_tag: "mute")
		s1.pc_slider(np("killsound", "ks"),
			proc {|k, v| s.cl(:tf_dingaling_lasthit_volume, v)}, 10, zero_tag: "mute")
	end
end
w.subsection(np("cl_imported")) do |s|
	s.action(np("drop_item", "dp_i"), s.cl(:dropitem))
	s.action(np("open_backpack", "o_bp"), s.cl(:open_charinfo_backpack))
	s.action(np("open_crafting_menu", "o_crft"), s.cl(:open_charinfo_crafting))
	s.action(np("open_loadout", "o_ldo"), s.cl(:open_charinfo_direct))
	s.action(np("open_party_chat", "o_pt_c"), s.cl(:empty_say_party)) # Broken due to the same bug as ..open_chat*
	s.action(np("pickup_item", "pkup_i"),
		mcl(s.cl(:plus_use_action_slot_item), s.cl(:wait, 1), s.cl(:minus_use_action_slot_item)))
	s.action(np("show_taunts", "s_tnt"), mcl(s.cl(:plus_taunt), s.cl(:wait, 1), s.cl(:minus_taunt)))
end
w.subsection(np("control", "c")) do |s|
	s.action(np("jump", "jmp"), mcl(s.cl(:plus_jump), s.cl(:wait, 2), s.cl(:minus_jump)))
	s.action(np("reload", "rl"), mcl(s.cl(:plus_reload), s.cl(:wait, 2), s.cl(:minus_reload)))
	s.action(np("use_special", "spcl"), mcl(s.cl(:plus_attack3), s.cl(:wait, 2), s.cl(:minus_attack3)))
	disguise = s.subsection(np("disguise", "dis")) do |s1|
		s1.action(np("demoman", "dm"), s.cl(:disguise, 4, -1))
		s1.action(np("engineer", "ng"), s.cl(:disguise, 9, -1))
		s1.action(np("heavy", "hv"), s.cl(:disguise, 6, -1))
		s1.action(np("medic", "md"), s.cl(:disguise, 5, -1))
		s1.action(np("pyro", "py"), s.cl(:disguise, 7, -1))
		s1.action(np("scout", "sc"), s.cl(:disguise, 1, -1))
		s1.action(np("sniper", "sn"), s.cl(:disguise, 2, -1))
		s1.action(np("soldier", "sl"), s.cl(:disguise, 3, -1))
		s1.action(np("spy", "sp"), s.cl(:disguise, 8, -1))
		own = s1.subsection(np("own", "o")) do |s2|
			s2.action(np("demoman", "dm"), s.cl(:disguise, 4, -2))
			s2.action(np("engineer", "ng"), s.cl(:disguise, 9, -2))
			s2.action(np("heavy", "hv"), s.cl(:disguise, 6, -2))
			s2.action(np("medic", "md"), s.cl(:disguise, 5, -2))
			s2.action(np("pyro", "py"), s.cl(:disguise, 7, -2))
			s2.action(np("scout", "sc"), s.cl(:disguise, 1, -2))
			s2.action(np("sniper", "sn"), s.cl(:disguise, 2, -2))
			s2.action(np("soldier", "sl"), s.cl(:disguise, 3, -2))
			s2.action(np("spy", "sp"), s.cl(:disguise, 8, -2))
		end
		own.gen_sub_data(["control", "disguise", "own"])
		own.action(np("demo"), own.cl("control.disguise.own.demoman*"))
		own.action(np("engie"), own.cl("control.disguise.own.engineer*"))
		own.action(np("solly"), own.cl("control.disguise.own.soldier*"))
	end
	disguise.gen_sub_data(["control", "disguise"])
	disguise.action(np("demo"), disguise.cl("control.disguise.demoman*"))
	disguise.action(np("engie"), disguise.cl("control.disguise.engineer*"))
	disguise.action(np("none", "no"), disguise.cl("control.disguise.own.spy*"))
	disguise.action(np("solly"), disguise.cl("control.disguise.soldier*"))
	disguise.gen_sub_data(["control", "disguise"])
	disguise.roulette(np("at_pace", "ap"), [
		disguise.cl("control.disguise.pyro*"),
		disguise.cl("control.disguise.engie*"),
		disguise.cl("control.disguise.medic*"),
		disguise.cl("control.disguise.sniper*"),
		disguise.cl("control.disguise.spy*")
	])
end
w.subsection(np("&gameplay", "&gp")) do |s|
	s.value(np("random_crits", "rnd_ct"),
		proc {|k, v| mcl(s.cl(:tf_weapon_criticals, v[0]), s.cl(:tf_weapon_criticals_melee, v[1]))},
		{"off" => [0, 0], "melee" => [0, 2], "ranged" => [1, 0], "on" => [1, 2]})
end
w.subsection(np("hud", "h")) do |s|
	s.action(np("class_select", "cl_sel"), s.cl(:changeclass))
	s.action(np("team_select", "tm_sel"), s.cl(:changeteam))
	s.plus_bool(np("scoreboard", "scoreboard"), :plus_scores)
	# .left_hand_disguise%: takes effect immediately but doesn't change visible UI
	s.wrapped_bool(np("left_hand_disguise", "lh_dis"), :tf_simple_disguise_menu, init: true)
	s.wrapped_bool(np("render_class_icon", "3d_clico"), :cl_hud_playerclass_use_playermodel)
	s.slider(np("chievo_glow_time", "ach_gl"),
		proc {|k, v| s.cl(:hud_achievement_glowtime, v)}, 1..5, 1, proc {|n| ["#{n}s", n]})
	# .mark_heal_target%: takes effect on next heal target (must be a different ally with autoheal)
	s.wrapped_bool(np("mark_heal_target", "hl_tgt"), :hud_medichealtargetmarker, init: true)
	# .colourblind_mode%: said to show jarate/milk overhead (like dominations), doesn't
#	s.wrapped_bool(np("colourblind_mode", "clrbl"), :tf_colorblindassist, init: true)
	s.subsection(np("indicators", "in")) do |s1|
		s1.wrapped_bool(np("enabled", "en"), :hud_combattext, init: true)
=begin
		hud_combattext_batching BOOL
		hud_combattext_batching_window FLOAT
		hud_combattext_doesnt_block_overhead_text BOOL
		hud_combattext_healing BOOL
		hud_combattext_[rgb] INT
=end
	end
	s.subsection(np("reticle", "ret")) do |s1|
		s1.wrapped_bool(np("hide_on_scope", "h_onsc"), :tf_hud_no_crosshair_on_scope_zoom, init: true)
		s1.slider(np("blue", "b"),
			proc {|k, v| s.cl(:cl_crosshair_blue, v)}, -1..255, 16, proc {|n| n < 0 ? ["00", 0] : ["%02X"%n, n]})
		s1.slider(np("green", "g"),
			proc {|k, v| s.cl(:cl_crosshair_green, v)}, -1..255, 16, proc {|n| n < 0 ? ["00", 0] : ["%02X"%n, n]})
		s1.value(np("image", "img"), proc {|k, v| s.cl(:cl_crosshair_file, v)}, {"circle" => "crosshair3",
			"cross" => "crosshair4", "cutoff_plus" => "crosshair2", "dot" => "crosshair5", "hl2" => "default",
			"hollow_plus" => "crosshair6", "small_plus" => "crosshair7", "dotted_plus" => "crosshair1"})
		s1.slider(np("red", "r"),
			proc {|k, v| s.cl(:cl_crosshair_red, v)}, -1..255, 16, proc {|n| n < 0 ? ["00", 0] : ["%02X"%n, n]})
		s1.slider(np("size", "sz"), proc {|k, v| s.cl(:cl_crosshair_scale, v)}, 4..96, 4, SELF_TWICE)
	end
	s.subsection(np("scoreboard", "scb")) do |s1|
		s1.wrapped_bool(np("exact_latency", "x_l8nc"), :tf_scoreboard_ping_as_text, init: true)
		s1.wrapped_bool(np("ugly_icons", "ug_ico"), :tf_scoreboard_alt_class_icons)
		# .unlock_cursor%: takes effect next time the scoreboard is shown
		s1.wrapped_bool(np("unlock_cursor", "unl_cs"), :tf_scoreboard_mouse_mode)
	end
end
w.subsection(np("input", "in")) do |s|
	s.subsection(np("mouse", "m")) do |s1|
		s1.wrapped_bool(np("filter", "fltr"), :m_filter)
	end
end
nextbot = w.subsection(np("&nextbot", "&nb")) do |s|
	s.create_bool(np("maintain_at_max", "mtn_mx"),
		s.cl(:tf_bot_quota_mode, "normal"), s.cl(:tf_bot_quota_mode, "fill"))
	s.slider(np("limit", "lim"), proc {|k, v| s.cl(:tf_bot_quota, v)}, 0..24, 2, SELF_TWICE)
	s.wrapped_bool(np("melee_only", "mle_o"), :tf_bot_melee_only)
	s.value(np("respawn_as", "rsp_as"), proc {|k, v| s.cl(:tf_bot_force_class, v)},
		{"demo" => "demoman", "engie" => "engineer", "heavy" => "heavyweapons", "medic" => "medic",
			"pyro" => "pyro", "scout" => "scout", "sniper" => "sniper", "solly" => "soldier", "spy" => "spy"})
	s.subsection(np("navmesh", "msh")) do |s1|
		s1.action(np("!autogen", "!au_gn"), s.cl(:nav_generate))
		s1.action(np("e_autogen", "e_au_gn"), mcl(s.cl(:sv_cheats, 1), s.cl(:nav_generate), s.cl(:sv_cheats, 0)))
		s1.action(np("!save", "!sv"), s.cl(:nav_save))
	end
end
nextbot.gen_sub_data(["&nextbot"])
nextbot.slider(np("limit_perteam", "lim_pt"),
	proc {|k, v| nextbot.cl("&nextbot.limit=#{v}")}, 0..24, 2, proc {|n| [n / 2, n]})
nextbot.gen_sub_data(["&nextbot"])
nextbot.action(np("maintain_4v4", "mtn_4"),
	mcl(nextbot.cl("&nextbot.limit_perteam=4"), nextbot.cl("&nextbot.maintain_at_max+")))
nextbot.action(np("maintain_6v6", "mtn_6"),
	mcl(nextbot.cl("&nextbot.limit_perteam=6"), nextbot.cl("&nextbot.maintain_at_max+")))
nextbot.action(np("maintain_9v9", "mtn_9"),
	mcl(nextbot.cl("&nextbot.limit_perteam=9"), nextbot.cl("&nextbot.maintain_at_max+")))
nextbot.action(np("maintain_12v12", "mtn_12"),
	mcl(nextbot.cl("&nextbot.limit_perteam=12"), nextbot.cl("&nextbot.maintain_at_max+")))
nextbot.gen_sub_data(["&nextbot"])
nextbot.action(np("spawn_4v4", "spn_4"),
	mcl(nextbot.cl("&nextbot.maintain_4v4*"), nextbot.cl("&nextbot.maintain_at_max-")))
nextbot.action(np("spawn_6v6", "spn_6"),
	mcl(nextbot.cl("&nextbot.maintain_6v6*"), nextbot.cl("&nextbot.maintain_at_max-")))
nextbot.action(np("spawn_9v9", "spn_9"),
	mcl(nextbot.cl("&nextbot.maintain_9v9*"), nextbot.cl("&nextbot.maintain_at_max-")))
nextbot.action(np("spawn_12v12", "spn_12"),
	mcl(nextbot.cl("&nextbot.maintain_12v12*"), nextbot.cl("&nextbot.maintain_at_max-")))
w.subsection(np("optimization")) do |s|
	s.raw_cmd(s.cl(:cl_threaded_client_leaf_system, 1))
	s.raw_cmd(s.cl(:r_drawopaquestaticpropslast, 1))
	s.raw_cmd(s.cl(:r_fastzreject, -1))
	s.raw_cmd(s.cl(:r_queued_decals, 1))
	s.raw_cmd(s.cl(:r_threaded_client_shadow_manager, 1))
	s.raw_cmd(s.cl(:r_threaded_renderables, 1))
end
w.subsection(np("&sv_imported")) do |s|
	s.value(np("holiday", "hday"),
		proc {|k, v| s.cl(:tf_forced_holiday, v)}, {"none" => 0, "birthday" => 1, "halloween" => 2, "smissmas" => 3})
=begin
	host_timescale
=end
end
w.gen_sub_data
WRITABLE_DATA[:TF2][:tress_tf2] << w.subs << w.gen_shorthand + [w.cl(:echo, "[TRESS|P] Finished loading.")]

{:x_nextbot_kickall => :tf_bot_kick, :x_nextbot_killall => :tf_bot_kill}.each do |k, v|
	w = AliasSection.new(k, np(nil), new_type_hash) do |w|
		w.subsection(np(nil)) {|s| VERSIONED_MISC[:TF2_NEXTBOT_NAMES].each {|n| s.raw_cmd(s.cl(v, n))}}
	end
	w.gen_sub_data
	WRITABLE_DATA[:TF2][k] << w.subs
end
