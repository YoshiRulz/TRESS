update_tdata(:TF2, "sane_defaults", [:tress_tf2, :sane_defaults])
update_tdata(:TF2, "sane_defaults_sv", [:tress_tf2, :sane_defaults_sv])

v = AliasSection.new(:sane_defaults_sv, np(nil), new_type_hash) do |v|
	v.subsection(np("admin")) do |s|
		s.raw_cmd("&admin.autobalance=ask")
		s.raw_cmd("&admin.autobalance_diff=2")
	end
	v.subsection(np("gameplay")) do |s|
		s.raw_cmd("&gameplay.random_crits=off")
	end
end
v.gen_sub_data
WRITABLE_DATA[:TF2][:sane_defaults_sv] << v.subs

w = AliasSection.new(:sane_defaults, np(nil), new_type_hash) do |w|
	w.subsection(np("admin")) do |s|
		s.raw_cmd("&admin.autobalance=ask")
		s.raw_cmd("&admin.autobalance_diff=2")
	end
	w.subsection(np("gameplay")) do |s|
		s.raw_cmd("&gameplay.random_crits=off")
	end
	w.subsection(np("audio")) do |s|
		s.raw_cmd("audio.require_focus+")
		s.raw_cmd("audio.captions=none")
		s.raw_cmd("audio.quality_mirror=high")
		s.raw_cmd("audio.speaker_config=2")
		s.subsection(np("hitsound")) do |s1|
			s1.raw_cmd("audio.hitsound.enabled+")
			s1.raw_cmd("# audio.hitsound.max_pitch=TODO")
			s1.raw_cmd("# audio.hitsound.min_pitch=TODO")
			s1.raw_cmd("# audio.hitsound.sound=TODO")
		end
		s.subsection(np("killsound")) do |s1|
			s1.raw_cmd("audio.killsound.enabled+")
			s1.raw_cmd("# audio.killsound.max_pitch=TODO")
			s1.raw_cmd("# audio.killsound.min_pitch=TODO")
			s1.raw_cmd("# audio.killsound.sound=TODO")
		end
		s.subsection(np("mixer")) do |s1|
			s1.raw_cmd("audio.mixer.hitsound=100%")
			s1.raw_cmd("audio.mixer.killsound=100%")
			s1.raw_cmd("audio.mixer.master=100%")
			s1.raw_cmd("audio.mixer.music=100%")
			s1.raw_cmd("audio.mixer.voice_chat=50%")
		end
	end
	w.subsection(np("cl_imported")) do |s|
		s.raw_cmd("cl_imported.hide_wep_wheel+")
		s.raw_cmd("cl_imported.display_gamma=2.4")
		s.raw_cmd("cl_imported.max_framerate=60")
	end
	w.subsection(np("graphics")) do |s|
		s.raw_cmd("graphics.3d_skybox+")
	end
	w.subsection(np("hud")) do |s|
		s.raw_cmd("hud.render_class_icon-")
		s.raw_cmd("hud.fov=90")
		s.raw_cmd("hud.chievo_glow_time=2s")
		s.raw_cmd("hud.mark_heal_target+")
		s.subsection(np("indicators")) do |s1|
			s1.raw_cmd("hud.indicators.enabled+")
		end
		s.subsection(np("reticle")) do |s1|
			s1.raw_cmd("hud.reticle.hide_on_scope+")
			s1.raw_cmd("hud.reticle.blue=FF")
			s1.raw_cmd("hud.reticle.green=FF")
			s1.raw_cmd("hud.reticle.image=dot")
			s1.raw_cmd("hud.reticle.red=7F")
			s1.raw_cmd("hud.reticle.size=16")
		end
		s.subsection(np("scoreboard")) do |s1|
			s1.raw_cmd("hud.scoreboard.exact_latency+")
			s1.raw_cmd("hud.scoreboard.ugly_icons-")
			s1.raw_cmd("hud.scoreboard.unlock_cursor-")
		end
	end
	w.subsection(np("input")) do |s|
		s.subsection(np("mouse")) do |s1|
			s1.raw_cmd("input.mouse.filter+")
			s1.raw_cmd("input.mouse.raw+")
		end
	end
	w.subsection(np("network")) do |s|
		s.raw_cmd("network.dl_from_host=nosounds")
		s.raw_cmd("network.max_rx_speed=500kbps")
	end
end
w.gen_sub_data
WRITABLE_DATA[:TF2][:sane_defaults] << w.subs
