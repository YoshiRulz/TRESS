VERSIONS[:GMod] = [18, 10, 17]

VERSIONED_COMMANDS[:GMod] = {
	cl_new_impact_effects: Builtin::BOOL_F,
	cl_phys_props_enable: Builtin::BOOL_T,
	m_filter: Builtin::BOOL_F,
	minus_attack3: [Builtin::PLUS, :plus_attack3],
	r_drawopaquestaticpropslast: Builtin::BOOL_F
}
VERSIONED_COMMANDS[:GMod].each do |k, v|
	if v[0].is_a? Builtin::PlusNoArg
		PLUS_MINUS_MAP[k] = v[1]
		PLUS_MINUS_MAP[v[1]] = k
	end
end
