update_tdata(:GMod, "tress_canonical", [:tress, :tress_gmod])
ALIASES[:tress_gmod] << "tress.roulette_intern_call^" # Overwritten by call to update_tdata

w = AliasSection.new(:tress_gmod, np(nil), new_type_hash)
w.subsection(np("input", "in")) do |s|
	s.subsection(np("mouse", "m")) do |s1|
		s1.wrapped_bool(np("filter", "fltr"), :m_filter)
	end
end
w.subsection(np("optimization")) do |s|
	s.raw_cmd(s.cl(:r_drawopaquestaticpropslast, 1))
end
w.gen_sub_data
WRITABLE_DATA[:GMod][:tress_gmod] << w.subs << w.gen_shorthand + [w.cl(:echo, "[TRESS|P] Finished loading.")]
