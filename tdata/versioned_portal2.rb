VERSIONS[:Portal2] = [2, 0, 0, 1]

VERSIONED_COMMANDS[:Portal2] = {
}
VERSIONED_COMMANDS[:Portal2].each do |k, v|
	if v[0].is_a? Builtin::PlusNoArg
		PLUS_MINUS_MAP[k] = v[1]
		PLUS_MINUS_MAP[v[1]] = k
	end
end
