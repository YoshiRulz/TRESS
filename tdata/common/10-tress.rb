SELF_TWICE = proc {|s| [s, s]}

update_tdata(:C, "tress_canonical", [:tress])
ALIASES[:tress] << "tress.roulette_intern_call^" # Overwritten by call to update_tdata

w = AliasSection.new(:tress, np(nil), new_type_hash)
noop = w.subsection(np("noop")) {|s| s.raw_cmd(ad(s.ns, "tress.noop*", s.cl(:find, "`")))}
noop.gen_sub_data([])
# noop* (#) can be used as a line comment, like in Ruby -- this form is preferred to SourceCon comments (//)
noop.raw_cmd(ad(noop.ns, "#", noop.cl("tress.noop*")))
w.subsection(np("&admin", "&ad")) do |s|
	s.action(np("cancel_presetup", "cnc_ps"), s.cl(:mp_waitingforplayers_cancel, 1))
end
w.subsection(np("audio", "au")) do |s|
	s.wrapped_bool(np("require_focus", "req_fc"), :snd_mute_losefocus, init: true, swap: true)
	s.value(np("captions", "cc"), proc {|k, v| mcl(s.cl(:closecaption, v[0]), s.cl(:cc_subtitles, v[1]))},
		{"none" => [0, 0], "cc" => [1, 0], "subs" => [1, 1]})
	s.value(np("quality_mirror", "ql_mir"), proc {|k, v| mcl(s.cl(:snd_pitchquality, v[0]),
		s.cl(:dsp_slow_cpu, v[1]))}, {"high" => [1, 0], "medium" => [0, 0], "low" => [0, 1]})
	s.value(np("speaker_config", "sp_cnf"), proc {|k, v| s.cl(:snd_surround_speakers, v[0])}, {"2" => 2})
	s.subsection(np("mixer", "mx")) do |s1|
		s1.pc_slider(np("master", "mst"), proc {|k, v| s.cl(:volume, v)}, 10, zero_tag: "mute")
		s1.pc_slider(np("music", "mus"), proc {|k, v| s.cl(:snd_musicvolume, v)}, 10, zero_tag: "mute")
		s1.pc_slider(np("voice_chat", "vc"), proc {|k, v| s.cl(:voice_scale, v)}, 10, zero_tag: "mute")
	end
end
w.subsection(np("cl_imported")) do |s|
	# Broken due to a bug, ValveSoftware/Source-1-Games#2632 on GitHub
	s.action(np("open_chat", "o_c"), s.cl(:empty_say))
	s.action(np("open_team_chat", "o_tm_c"), s.cl(:empty_say_team)) # Broken due to the same bug as ..open_chat*
	s.wrapped_bool(np("hide_wep_wheel", "h_ww"), :hud_fastswitch, swap: true)
	s.slider(np("display_gamma", "disp_gm"),
		proc {|k, v| s.cl(:mat_monitorgamma, v)}, 16..26, 1, proc {|n| (0..1).map {|i| "%03.1f"%(n / 10.0)}}, init: 2.2)
	s.slider(np("max_framerate", "mx_fps"), proc {|k, v| s.cl(:fps_max, v)}, 30..150, 5, SELF_TWICE, init: 60)
	s.action(np("exploding_pumpkin", "xpl_pmp"), s.cl(:ent_create, "tf_pumpkin_bomb"))
=begin
	exec
	lastinv
	m_pitch +0.022
	m_pitch -0.022
	m_customaccel
	m_customaccel_exponent
	joy_movement_stick INT
	joy_pitchsensitivity
	joy_yawsensitivity
	voice_modenable BOOL
	showmapinfo
	cl_decline_first_notification
	cl_trigger_first_notification
	// Our patented* Medicinal (extraterrestrials didn't interfere) Distance and Ranging technology!
	// That's MediDAR for short.
	// *patent pending
	alias +cs_medidar "hud_medicautocallersthreshold 450"
	alias -cs_medidar "hud_medicautocallersthreshold 50"
	cl_notifications_show_ingame 0
	hud_medicautocallers 1
	cl_ask_favorite_opt_out 1
	cl_burninggibs 1
	cl_phys_props_enable 0
	cl_showbackpackrarities 1
	cl_trading_show_requests_from 2
	hud_classautokill 0
	tf_remember_activeweapon 1
	tf_remember_lastswitched 1
	cl_localnetworkbackdoor 0
	cl_smooth 0
	cl_timeout 5
	ai_expression_optimization 1
	mat_hdr_level 0
	r_drawtracers_firstperson 0
	cl_new_impact_effects 1
	mat_disable_bloom 1
	mat_motion_blur_enabled 0
	r_flashlightdepthtexture 0
	r_shadowmaxrendered 16
	tf_sniper_fullcharge_bell 1
	cl_yawspeed 200
	cl_interp 0
	cl_interp_ratio 2
	sv_forcepreload 1
	sv_password get_off_my_server
	tf_bot_difficulty 3
	lastdisguise
=end
=begin
	viewmodel_fov INT
	cl_cmdrate 10..100 # def 30 archived?
	cl_updaterate 20 # archived?
	tf_ctf_bonus_time FLOAT
	tf_flag_caps_per_round INT
	mp_disable_respawn_times BOOL
	cl_autoreload INT
	zoom_sensitivity_ratio FLOAT
	cl_autorezoom BOOL
	cl_hud_killstreak_display_time FLOAT
	tf_medigun_autoheal BOOL
=end
=begin
	spam jump
	hooks
	toggle movement and attack
=end
end
w.subsection(np("console", "con")) do |s|
	s.action(np("clear", "cl"), s.cl(:clear))
	s.action(np("show_var_diff", "var_df"), s.cl(:differences))
end
w.subsection(np("control", "c")) do |s|
	s.action(np("gib_suicide", "g_sui"), s.cl(:explode))
	s.action(np("suicide", "sui"), s.cl(:kill))
	s.plus_bool(np("alt_fire", "a_fr"), :plus_attack2)
	s.plus_bool(np("backward", "bw"), :plus_back)
	s.plus_bool(np("crouch", "cr"), :plus_duck)
	s.plus_bool(np("fire", "fr"), :plus_attack)
	s.plus_bool(np("forward", "fw"), :plus_forward)
	s.create_bool(np("!noclip", "!nc"), s.cl(:noclip), s.cl(:noclip))
	s.plus_bool(np("strafe_left", "s_l"), :plus_moveleft)
	s.plus_bool(np("strafe_right", "s_r"), :plus_moveright)
	s.plus_bool(np("turn_left", "t_l"), :plus_left)
	s.plus_bool(np("turn_right", "t_r"), :plus_right)
end
w.subsection(np("graphics", "gfx")) do |s|
	s.wrapped_bool(np("3d_skybox", "3dsb"), :r_3dsky)
end
w.subsection(np("hud", "h")) do |s|
	s.slider(np("fov", "fov"), proc {|k, v| s.cl(:fov_desired, v)}, 70..90, 1, SELF_TWICE)
	s.create_bool(np("show_framerate", "s_fps"), s.cl(:cl_showfps, 0), s.cl(:cl_showfps, 2))
	s.value(np("console_overlay", "con_ovr"), proc {|k, v| s.cl(:developer, v)},
		{"none" => 0, "info" => 1, "debug" => 2})
end
w.subsection(np("input", "in")) do |s|
	gamepad = s.subsection(np("gamepad", "gp")) do |s1|
		s1.action(np("disable", "dis"), s.cl(:joystick, 0))
		s1.action(np("enable", "en"), s.cl(:joystick, 1))
		s1.wrapped_bool(np("invert_pitch", "i_pt"), :joy_inverty)
	end
	gamepad.gen_sub_data(["input", "gamepad"])
	gamepad.create_bool(np("toggle_enabled", "tgl_en"),
		gamepad.cl("input.gamepad.disable*"), gamepad.cl("input.gamepad.enable*"))
	s.subsection(np("mouse", "m")) do |s1|
		s1.wrapped_bool(np("raw", "raw"), :m_rawinput)
	end
end
w.subsection(np("network", "nw")) do |s|
	s.action(np("disconnect", "dc"), s.cl(:disconnect))
	s.value(np("dl_from_host", "dl_f_hs"),
		proc {|k, v| s.cl(:cl_downloadfilter, k)}, {"all" => nil, "nosounds" => nil, "mapsonly" => nil, "none" => nil})
	# .max_rx_speed: binary kbps (1 kbps = 1024 b/s = 128 B/s)
	s.value(np("max_rx_speed", "rx_spd"), proc {|k, v| s.cl(:rate, v << 10)}, {
		"50kbps" => 50, "75kbps" => 75, "100kbps" => 100, "150kbps" => 150, "200kbps" => 200, "250kbps" => 250,
		"300kbps" => 300, "400kbps" => 400, "500kbps" => 500, "600kbps" => 600, "700kbps" => 700, "800kbps" => 800,
		"900kbps" => 900, "1mbps" => 1000}, init: "100kbps")
end
w.subsection(np("optimization")) do |s|
	# these possibly do nothing
	s.raw_cmd(s.cl(:dsp_enhance_stereo, 1))
	s.raw_cmd(s.cl(:fast_fogvolume, 1))
	s.raw_cmd(s.cl(:mat_queue_mode, 2))
	s.raw_cmd(s.cl(:particle_sim_alt_cores, 4))
	s.raw_cmd(s.cl(:r_threaded_particles, 1))
	s.raw_cmd(s.cl(:snd_async_fullyasync, 1))
	s.raw_cmd(s.cl(:snd_mix_async, 1))
=begin
	s.raw_cmd(s.cl(:filesystem_max_stdio_read, 16)) # assumed KiB
	s.raw_cmd(s.cl(:datacachesize, 512)) # 32..512 def 64, interpreted as MiB
=end

	# these definitely do nothing, or at least not what their name and documentation suggest
	# s.raw_cmd(s.cl(:cl_blobbyshadows, 1)) # no visible effects when changed in-game, may take effect on next map load
	# s.raw_cmd(s.cl(:r_decal_cullsize, 5))
	# s.raw_cmd(s.cl(:ragdoll_sleepaftertime, "5.0f"))
end
w.subsection(np("tress")) do |s|
	s.throwable(np("roulette_intern_call"), "Roulette internal was called externally and w/o starting roulette")
	s.raw_print(np("version?"), "tress_canonical built by TRESS v#{VERSIONS[:TRESS] * "."}-#{VERSIONS[:TF2] * "."}")
	s.subsection(np("help")) do |s1|
		s1.raw_help(np("shorthand?"), "shorthand explanation", ["Shorthand commands are identical to their corresponding long form with a few exceptions.", "All syntax is copied from the long form, but * is optional and +, -, and % are only prefixes, as opposed to either prefixes or suffixes.", "Individual commands have no help? or examples??, groups have a help? which gives their full form.", "See .shorthand?? for a list of the shorthand groups."])
		s1.raw_help(np("shorthand??"), "shorthand group list", [w.sub_a[:subsections].values.reject {|s| s.names.length == 1}.map {|s| "#{s.name} -> #{s.names[1]}"} * ", "])
		s1.raw_help(np("syntax??"), "concise explanation of added syntax", ["one_off_action* enable_or_start+ disable_or_stop- toggle% set_value=", "set.subcommand &host_cmd cheat!", "list_commands| help? examples?? &all.!@at_once+?", "throw_error^ throw_fatal^!", "@roulette_start+ @roulette_stop+ @roulette_state< @roulette_read> @roulette_intern@"])
	end
end
w.gen_sub_data
WRITABLE_DATA[:C][:tress] << w.subs << w.gen_shorthand + [w.cl(:echo, "[TRESS|P] Finished loading.")]
