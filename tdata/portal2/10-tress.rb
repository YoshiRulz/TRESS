update_tdata(:Portal2, "tress_canonical", [:tress, :tress_portal2])
ALIASES[:tress_portal2] << "tress.roulette_intern_call^" # Overwritten by call to update_tdata

w = AliasSection.new(:tress_portal2, np(nil), new_type_hash)
#TODO portal 2-specific commands
w.gen_sub_data
WRITABLE_DATA[:Portal2][:tress_portal2] << w.subs << w.gen_shorthand + [w.cl(:echo, "[TRESS|P] Finished loading.")]
